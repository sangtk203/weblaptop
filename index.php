<!--trang chinh -->
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Web Laptop</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css">
</head>
<body>
	<div class="wrapper">
		<?php
		//nhan du lieu tu cac file phu
		include("admincp/config/config.php");
		include("pages/header.php");
		include("pages/menu.php");
		include("pages/search.php");
		include("pages/main.php");
		include("pages/footer.php");
		?>	
	</div>
</body>
</html>