<!-- //trang admin quan ly du lieu -->
<?php
session_start();
if(!isset($_SESSION['dangnhap'])){
	header('Location:login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admincp</title>
	<link rel="stylesheet" type="text/css" href="css/styleadmincp.css">
</head>
<body>
	<h3 class="title_admin">AdminCP</h3>
	<div class="wrapper">
	<?php
		include("config/config.php");
		include("modules/header.php");
		include("modules/menu.php");
		include("modules/main.php");
		include("modules/footer.php");
	?>
	</div>
</body>
</html>